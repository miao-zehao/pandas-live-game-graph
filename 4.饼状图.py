# @Time    : 2022/1/13 5:57
# @Author  : 南黎
# @FileName: 4.饼状图.py

import pandas as pd

######显示中文宋体字体导入，如果使用中文加上这段代码######
import matplotlib as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#####################################################
import pandas_alive

df = pd.read_csv("数据源data/covid19.csv", index_col=0, parse_dates=[0], thousands=',')

df.plot_animated(filename='4.饼状图.gif',
                 title='发现你走远了——4.饼状图',
                 kind="pie",#图标类型为饼状图
                 rotatelabels=True,#是否可以旋转
                 period_label={'x': 0, 'y': 0})#坐标轴周期
