# @Time    : 2022/1/12 22:13
# @Author  : 南黎
# @FileName: 7.多个图表.py

import pandas as pd

######显示中文宋体字体导入，如果使用中文加上这段代码######
import matplotlib as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#####################################################

import pandas_alive

df = pd.read_csv("数据源data/covid19.csv", index_col=0, parse_dates=[0], thousands=',')

# 生成第一个表
animated_line_chart = df.diff().fillna(0).plot_animated(
    title='发现你走远了——子图1',
    kind='line',
    period_label=False,
    add_legend=False
)
# 生成第二个表
animated_bar_chart = df.plot_animated(
    title='发现你走远了——子图2',
    n_visible=10
)

# 最后保存图片的时候，用一个列表同时装入2个表
pandas_alive.animate_multiple_plots(
    filename='7.多个图表.gif',
    plots=[animated_bar_chart, animated_line_chart],  # 两个表放进一个列表中
    title='发现你走远了——7.多个图表',
    enable_progress_bar=True  # 是否在生成图片时显示生成图片的进度,以一个进度条的方式呈现
)
