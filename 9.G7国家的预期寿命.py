# @Time    : 2022/1/12 22:15
# @Author  : 南黎
# @FileName: 9.G7国家的预期寿命.py

import pandas as pd

######显示中文宋体字体导入，如果使用中文加上这段代码######
import matplotlib as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#####################################################

import pandas_alive
import pandas as pd

data_raw = pd.read_csv("数据源data/Life Expectancy in G7 Countries.csv")
# 原作者是在线获取，这里直接用已经下载好的csv
# data_raw = pd.read_csv(
#     "https://raw.githubusercontent.com/owid/owid-datasets/master/datasets/Long%20run%20life%20expectancy%20-%20Gapminder%2C%20UN/Long%20run%20life%20expectancy%20-%20Gapminder%2C%20UN.csv"
# )


list_G7 = [
    "Canada",
    "France",
    "Germany",
    "Italy",
    "Japan",
    "United Kingdom",
    "United States",
]
#重塑数据（产生一个“pivot”表格）以列值为标准。使用来自索引/列的唯一的值（去除重复值）为轴形成dataframe结果。
data_raw = data_raw.pivot(
    index="Year", columns="Entity", values="Life expectancy (Gapminder, UN)"
)

data = pd.DataFrame()
data["Year"] = data_raw.reset_index()["Year"]
#因为原始网页数据集有很多国家，这里选择我们需要的7个国家
for country in list_G7:
    data[country] = data_raw[country].values

data = data.fillna(method="pad")
data = data.fillna(0)
data = data.set_index("Year").loc[1900:].reset_index()

data["Year"] = pd.to_datetime(data.reset_index()["Year"].astype(str))

data = data.set_index("Year")

animated_bar_chart = data.plot_animated(
    period_fmt="%Y",#动态更新图中时间戳
    perpendicular_bar_func="mean", #设置平均值辅助线
    period_length=200,#周期长度 200ms
    fixed_max=True,

)

animated_line_chart = data.plot_animated(
    kind="line",
    period_fmt="%Y",
    period_length=200,
    fixed_max=True
)

pandas_alive.animate_multiple_plots(
    filename="9.G7国家的预期寿命.gif",
    plots=[animated_bar_chart, animated_line_chart],
    title="发现你走远了——9.G7国家的预期寿命",
    adjust_subplot_left=0.2,
    adjust_subplot_top=0.9,
    enable_progress_bar=True
)
