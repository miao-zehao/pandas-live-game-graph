# @Time    : 2022/1/12 19:58
# @Author  : 南黎
# @FileName: 5.气泡图.py

import pandas as pd

######显示中文宋体字体导入，如果使用中文加上这段代码######
import matplotlib as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#####################################################

import pandas_alive

multi_index_df = pd.read_csv("数据源data/multi.csv", header=[0, 1], index_col=0)

multi_index_df.index = pd.to_datetime(multi_index_df.index, dayfirst=True)

map_chart = multi_index_df.plot_animated(
    kind="bubble",
    filename="5.气泡图.gif",
    title='发现你走远了——5.气泡图',
    x_data_label="Longitude",  # x数据标签 使用数据集的Longitude属性
    y_data_label="Latitude",  # y数据标签 使用数据集的Latitude属性
    size_data_label="Cases",
    color_data_label="Cases",
    vmax=5,
    steps_per_period=3,#每个周期的步数
    interpolate_period=True, period_length=500,
    dpi=100
)
