# @Time    : 2022/1/12 19:57
# @Author  : 南黎
# @FileName: 1.3条形阶梯图.py
import pandas_alive
import pandas as pd

######显示中文宋体字体导入，如果使用中文加上这段代码######
import matplotlib as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#####################################################

df = pd.read_csv("数据源data/covid19.csv", index_col=0, parse_dates=[0], thousands=',')

df.sum(axis=1).fillna(0).plot_animated(filename='1.3条形阶梯图.gif',
                                       kind='bar',#图的类型
                                       period_label={'x': 0.1, 'y': 0.9},#坐标周期
                                       title='发现你走远了——1.3条形阶梯图',
                                       enable_progress_bar=True,
                                       steps_per_period=2,
                                       interpolate_period=True,
                                       period_length=200
                                       )
