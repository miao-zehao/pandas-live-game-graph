# @Time    : 2022/1/12 19:37
# @Author  : 南黎
# @FileName: 2.折线图.py
import pandas_alive
import pandas as pd
######显示中文宋体字体导入，如果使用中文加上这段代码######
import matplotlib as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#####################################################

df = pd.read_csv("数据源data/covid19.csv", index_col=0, parse_dates=[0], thousands=',')

# diff() 你就写diff()一般都够用。进阶的理解diff(X)：对于一个向量来说，diff(X)就是 [X(2)-X(1) X(3)-X(2) … X(n)-X(n-1)] 也就是说求相邻2点的距离。对于一个矩阵来说，结果是：[X(2:n,:) - X(1:n-1,:)]；对于一个N*D的矩阵，结果是后一行减前一行的差值。
# fillna(0) 空值设置为0
# period_label={'x':0.25,'y':0.9} 表示

df.diff().fillna(0).plot_animated(
    filename='2.折线图.gif',
    title='发现你走远了——2.折线图',
    kind='line',#图标类型为折线图
    period_label={'x': 0.25, 'y': 0.9}#x，y坐标的周期
)
