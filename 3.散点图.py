# @Time    : 2022/1/12 19:57
# @Author  : 南黎
# @FileName: 3..py

import pandas as pd

######显示中文宋体字体导入，如果使用中文加上这段代码######
import matplotlib as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#####################################################

import pandas as pd
import pandas_alive

max_temp_df = pd.read_csv(
    "数据源data/Newcastle_Australia_Max_Temps.csv",
    parse_dates={"Timestamp": ["Year", "Month", "Day"]},
)
min_temp_df = pd.read_csv(
    "数据源data/Newcastle_Australia_Min_Temps.csv",
    parse_dates={"Timestamp": ["Year", "Month", "Day"]},
)

merged_temp_df = pd.merge_asof(max_temp_df, min_temp_df, on="Timestamp")

merged_temp_df.index = pd.to_datetime(merged_temp_df["Timestamp"].dt.strftime('%Y/%m/%d'))

#设置图例内容
keep_columns = ["Minimum temperature (Degree C)", "Maximum temperature (Degree C)"]

merged_temp_df[keep_columns].resample("Y").mean().plot_animated(
    filename='3.散点图.gif',
    kind="scatter",#图标类型为散点图
    title='发现你走远了——3.散点图'
)
