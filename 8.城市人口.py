# @Time    : 2022/1/12 22:14
# @Author  : 南黎
# @FileName: 8.城市人口.py

import pandas as pd

######显示中文宋体字体导入，如果使用中文加上这段代码######
import matplotlib as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#####################################################

import pandas_alive

urban_df = pd.read_csv("数据源data/urban_pop.csv", index_col=0, parse_dates=[0], thousands=',')

animated_line_chart = (
    urban_df
        .sum(axis=1)  # 按行计算的和
        .pct_change()  # 计算与前一个元素的百分比 比如说[1,2,3].pct_change()=[NaN,(2-1)/1,(3-2)/2] 为什么第一个是NaN空值呢，因为第一数前面没有数给他减，给他除了···
        .fillna(method='bfill')  # backfill/bfill用下一个非缺失值填充该缺失值，因为前一步计算会有缺失值
        .mul(100)  # 缺失值单元格在相乘之前已填充100 函数返回数据帧和其他元素的乘法。此功能本质上与dataframe * other，但它提供了额外的支持来处理其中一个输入中的缺失值。
        .plot_animated(
        kind="line",  # 图表类型为折线图
        title="子图1",  # 子图名称
        period_label=False,  # 不显示时间
        add_legend=False  # 不显示渲染图进度条
    )

)

animated_bar_chart = urban_df.plot_animated(
    n_visible=10,  # 属性参数取10个
    title="子图2",  # 子图名称
    period_fmt="%Y"  # 时间格式，XXXX年
)

pandas_alive.animate_multiple_plots(
    filename='8.城市人口.gif',
    plots=[animated_bar_chart, animated_line_chart],  # 两个表放进一个列表中
    title='发现你走远了——8.城市人口',
    enable_progress_bar=True,
    adjust_subplot_top=0.85,

    # 设置子图的位置
    # adjust_subplot_left=0.5,
    # adjust_subplot_right=0.9,
    # adjust_subplot_bottom=0.1,
    # adjust_subplot_top=0.9,
    # adjust_subplot_wspace=0.2,
    # adjust_subplot_hspace=0.25
)
