# @Time    : 2022/1/14 21:04
# @Author  : 南黎
# @FileName: 6.2多边形地理空间图.py

import pandas as pd

######显示中文宋体字体导入，如果使用中文加上这段代码######
import matplotlib as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#####################################################

import geopandas
import pandas_alive
import contextily

gdf = geopandas.read_file('数据源data/italy-covid-region.gpkg')
gdf.index = gdf.region
gdf = gdf.drop('region', axis=1)

map_chart = gdf.plot_animated(
    filename='6.2多边形地理空间图.gif',
    title='发现你走远了——6.2多边形地理空间图',
    enable_progress_bar=True,
    basemap_format={'source': contextily.providers.Stamen.Terrain}
)
