# @Time    : 2022/1/12 13:24
# @Author  : 南黎
# @FileName: 1.2竖直条形图.py
import pandas_alive
import pandas as pd

######显示中文宋体字体导入，如果使用中文加上这段代码######
import matplotlib as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#####################################################

df = pd.read_csv("数据源data/Aus_Elec_Gen_1980_2018.csv", index_col=0, parse_dates=[0], thousands=',')

df.plot_animated(filename='1.2竖直条形图.gif', title='发现你走远了——1.2竖直条形图', orientation='v')
