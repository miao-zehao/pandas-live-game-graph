# @Time    : 2022/1/12 11:46
# @Author  : 南黎
# @FileName: 1.1水平条形图.py
import pandas as pd
import pandas_alive

######显示中文宋体字体导入，如果使用中文加上这段代码######
import matplotlib as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
#####################################################

df = pd.read_csv("数据源data/Aus_Elec_Gen_1980_2018.csv", index_col=0, parse_dates=[0], thousands=',')

df.fillna(0).plot_animated('1.1水平条形图.gif', period_fmt="%Y-%m-%d",
                           title='发现你走远了——1.1水平条形图',
                           fixed_max=False,
                           perpendicular_bar_func='mean',
                           fixed_order=False
                           )
